function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
         j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1]) #preste atenção nesta linha
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function ValueOfCard(card)

    if card[1] == '2'
        card = 2
    end

    if card[1] == '3'
        card = 3
    end

    if card[1] == '4'
        card = 4
    end

    if card[1] == '5'
        card = 5
    end

    if card[1] == '6'
        card = 6
    end

    if card[1] == '7'
        card = 7
    end

    if card[1] == '8'
        card = 8
    end

    if card[1] == '9'
        card = 9
    end

    if card[1] == '1'
        card = 10
    end

    if card[1] == 'J'
        card = 11
    end

    if card[1] == 'Q'
        card = 12
    end

    if card[1] == 'K'
        card = 13
    end

    if card[1] == 'A'
        card = 14
    end

    return card

end

function compareByValue(x, y)

    x = ValueOfCard(x)
    y = ValueOfCard(y)

    if x < y
        return true
    else
        return false
    end

end

using Test

function testacompareByValue()

    @test compareByValue("2♠", "A♠")
    @test !compareByValue("K♥", "10♥")
    @test compareByValue("10♥", "A♠")
    @test !compareByValue("10♠", "10♥")

    println("final dos testes")

end

function testainsercao1()

    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "2♠"]) == ["2♠","10♥","10♦","J♠","K♠","A♠"]
    @test insercao(["Q♥", "10♦", "2♠", "A♠", "J♠", "K♠"]) == [ "2♠","10♦","J♠","Q♥","K♠","A♠"]
    @test insercao(["K♥"]) == ["K♥"]
    @test insercao([]) == []

    println("final dos testes")

end

function SuitOfCard(cards)
    
    tam = length(cards)

    if cards[tam] == '♢'
        cards = 1

    elseif cards[tam] == '♠'
        cards = 2

    elseif cards[tam] == '♡'
        cards = 3

    elseif cards[tam] == '♣'
        cards = 4
    end

    return cards

end

function compareByValueAndSuit(x, y)

    suitX = SuitOfCard(x)
    suitY = SuitOfCard(y)

    if suitX < suitY
        return true
    end

    if suitX > suitY
        return false
    end

    if suitX == suitY
        compareByValue(x, y)
    end

end

function testacompareByValueAndSuit()
    @test compareByValueAndSuit("10♡", "10♣")
    @test compareByValueAndSuit("A♢", "10♣")
    @test compareByValueAndSuit("10♡", "A♡")
    @test !compareByValueAndSuit("K♠", "10♠")

    println("final dos testes")

end

function testainsercao2()
    @test insercao(["10♡", "10♢", "K♠", "A♠", "J♠", "A♠"]) == ["10♢","J♠","K♠","A♠","A♠","10♡"]
    @test insercao(["J♡", "A♢", "K♠", "2♣"]) == ["A♢","K♠","J♡","2♣"]
    @test insercao(["A♢"]) == ["A♢"]
    @test insercao([]) == []

    println("final dos testes")

end